let
  Prelude = ./deps/Prelude.dhall
let
  Map = Prelude.Map
let
  CF = ./deps/Containerfile.dhall
let
  Image = ./Image.dhall

let
  images: List Image.Type = ./images.dhall
let
  DockerfileDir: Type = { Dockerfile : Text }
let
  toDockerfileDir: Image.Type -> Map.Entry Text DockerfileDir =
    \(cf: Image.Type) -> Map.keyValue DockerfileDir cf.name { Dockerfile = CF.render cf.image }

in Prelude.List.map Image.Type (Map.Entry Text DockerfileDir) toDockerfileDir images